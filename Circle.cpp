/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
namespace Student1 
{
Circle::Circle(double r) {
	setR(r);
}
Circle::Circle(int r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r; 
}
void Circle::setR(int r) {
	this->r = r;
}
double Circle::getR(){
	return r;
}
double Circle::calculateCircumference(){
	return PI * r * 2;
}

double Circle::calculateArea(){
	return PI * r * r;

}
bool Circle::isequal(double a, double b)
{
	if (a == b)
		return true;
	else 
		return false;
}
bool Circle::isequal(int a, int b)
{
	if (a == b)
		return true;
	else
		return false;
}
}
